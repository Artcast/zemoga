import { combineReducers } from 'redux';
import participants from './participantReducer';

export default combineReducers({
    participants: participants
});