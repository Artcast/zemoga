import { FETCH_PARTICIPANTS } from '../actions/types';

export default function participantReducer(state = [], action) {
  switch (action.type) {
      case FETCH_PARTICIPANTS:
      return action.posts;
    default:
      return state;
  }
}