import { FETCH_PARTICIPANTS } from './types';
import axios from 'axios';

const apiUrl = 'http://localhost:3000/json/participants.json';

export const fetchParticipants = (participants) => {
  return {
    type: FETCH_PARTICIPANTS,
    posts
  }
};

export const fetchAllParticipants = () => {
  return (dispatch) => {
    return axios.get(apiUrl)
      .then(response => {
        dispatch(fetchParticipants(response.data))
      })
      .catch(error => {
        throw(error);
      });
  };
};