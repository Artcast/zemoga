import React from 'react';
import classes from './topContainer.module.scss';
import CardInfo from '../../../components/cardInfo/cardInfo';

const TopContainer = () => {
    return (
        <div className={classes.topContainer}>
            <CardInfo/>
            <div className={classes.bottom}>
                <div className={classes.close}>CLOSING IN</div>
                <div className={classes.closeValue}><span>22</span>&ensp;days</div>
            </div>
        </div>
    );
};

export default TopContainer;
