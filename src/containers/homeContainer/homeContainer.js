import React from 'react';
import MiddleContainer from './middleContainer/middleContainer';
import TopContainer from './topContainer/topContainer';

const HomeContainer = () => {
    return (
        <div>
            <TopContainer/>
            <MiddleContainer/>
        </div>
    );
};

export default HomeContainer;
