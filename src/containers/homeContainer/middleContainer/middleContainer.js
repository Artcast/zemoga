import React from 'react';
import Message from '../../../components/message/message';
import ListParticipants from '../../../components/listParticipants/listParticipants';
import AnyoneElse from '../../../components/anyoneElse/anyoneElse';
import classes from './middleContainer.module.scss';

const MiddleContainer = () => {
    return (
        <div className={classes.middleContainer}>
            <Message/>
            <ListParticipants/>
            <AnyoneElse/>
        </div>
    );
};

export default MiddleContainer;
