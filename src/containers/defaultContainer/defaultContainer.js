import React from 'react';
import classes from './defaultContainer.module.scss';
const HomeContainer = (props) => {
    return (
        <div className={classes.defaultContainer}>
            {props.title}
        </div>
    );
};

export default HomeContainer;
