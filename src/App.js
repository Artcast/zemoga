import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './pages/home/home';
import HowItWorks from './pages/howItWorks/howItWorks';
import PastTrial from './pages/pastTrial/pastTrial';
import Login from './pages/login/login';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/home" component={Home} />
          <Route path="/past-trial" component={PastTrial} />
          <Route path="/how-it-works" component={HowItWorks} />
          <Route path="/login" component={Login} />
        </Switch>
    </Router>
    );
  }
}

export default App;
