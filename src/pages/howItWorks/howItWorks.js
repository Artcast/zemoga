import React, { Component } from 'react';
import DefaultContainer from '../../containers/defaultContainer/defaultContainer';
import Template from '../../components/template/template';

class HowItWorks extends Component {

  render() {
    return (
      <Template container={<DefaultContainer title="How It Works"/>} />
    );
  }
}


export default HowItWorks;