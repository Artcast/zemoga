import React, { Component } from 'react';
import HomeContainer from '../../containers/homeContainer/homeContainer';
import Template from '../../components/template/template';

class Home extends Component {

  render() {
    return (
      <Template container={<HomeContainer/>} />
    );
  }
}


export default Home;
