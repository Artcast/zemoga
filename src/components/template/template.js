import React, { Component } from 'react';
import Header from '../header/header';
import Footer from '../footer/footer';
import classes from './template.module.scss';

class Template extends Component {
  render() {
    const { container } = this.props;
    return (
      <div className={classes.body}>
        <Header />    
        <main>
          <div className="content">
          {container}
          </div>
        </main>   
        <Footer /> 
      </div>
    );
  }
}

export default Template;