import React from 'react';
import classes from './header.module.scss';
import search from '../../assets/images/search.png';
import { NavLink, withRouter } from 'react-router-dom'
class Header extends React.Component{

    render(){
        return (
            <header>
                <div className={classes.header}>
                    <div className={classes.logo}>Rules of Thumbs.</div>
                    <nav>
                        <div className={classes.nav}>
                            <NavLink to="/past-trial">Past Trial</NavLink>
                            <NavLink to="/how-it-works">How it Works Trial</NavLink>
                            <NavLink to="/login">Log In / Sing Up</NavLink>                    
                            <div><img src={search} alt="Search" title="Search"/></div>
                        </div>
                    </nav>
                </div>
            </header>
        );
    }
    
};

export default withRouter(Header);