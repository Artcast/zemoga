import React from 'react';
import Button from '../button/button';
import classes from './anyoneElse.module.scss';

const AnyoneElse = () => {
    return (
        <div className={classes.anyoneElse}>
            <div className={classes.bg}></div>
            <div className={classes.message}>
                <div className={classes.title}>Is there anyone else  you would want  us to add?</div>
                <div className={classes.button}><Button label="Submit a Name"/></div>
            </div>
        </div>
    );
};

export default AnyoneElse;
