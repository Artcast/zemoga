import React from 'react';
import classes from './participant.module.scss';
import Button from '../../button/button';
import ButtonThumb from '../../buttonThumb/buttonThumb';
import ParticipantScore from './participantScore/participantScore';
import { URL_IMAGE } from '../../../constants/constants';


class Participant extends React.Component {
    constructor(props){
        super(props);
        this.currentThumb = (props.participant.thumbUp>props.participant.thumbDown)?'up':'down';
        this.style = {
            backgroundImage: `url(${URL_IMAGE+props.participant.img})`
        }
        this.state = {
            participant:this.props.participant,
            toggle:null,
            voting:false,
        }
    }

    handleToggle = (flag) => {
        this.setState({toggle:flag});
    }

    handleVote = () => {
        const { toggle, participant } = this.state;  
        if(toggle!==null){
            if(toggle){
                participant.thumbUp = participant.thumbUp+1;                
            }
            else{
                participant.thumbDown = participant.thumbDown+1;   
            }
            this.setState({participant: participant, voting: true });
            const lParticipants = JSON.parse(localStorage.getItem('lParticipants'));
            lParticipants[this.props.index] = participant;
            localStorage.setItem('lParticipants',JSON.stringify(lParticipants));
            this.currentThumb = (participant.thumbUp>participant.thumbDown)?'up':'down';
        }        
    }

    handleVoteAgain = () => {
        this.setState({voting:false,toggle:null});
    }



    render(){
        const { toggle, voting } = this.state;
        const {
            desc,
            fullName,
            sector,
            thumbDown,
            thumbUp,
            workMonthExp } = this.state.participant;  
        const vote = voting?(
            <div className={classes.voteContainer}>
                <Button onClick={()=>this.handleVoteAgain()} label="Vote again"/>
            </div>
        ):
        (
            <div className={classes.voteContainer}>
                <ButtonThumb onClick={()=>this.handleToggle(true)} border={(toggle && toggle!==null)} thumbWay="up"/>
                <ButtonThumb onClick={()=>this.handleToggle(false)} border={(!toggle && toggle!==null)} thumbWay="down"/>
                <Button onClick={()=>this.handleVote()} label="Vote now"/>
            </div>
        )
        const description = voting?'Thank you for voting!':desc;           
        return (
            <div className={classes.cardParticipant}>
                <div style={this.style} className={classes.avatar}></div>
                <div className={classes.info}>
                    <div>
                        <div className={classes.currentThumb}><ButtonThumb thumbWay={this.currentThumb}/></div>
                        <div className={classes.fullname}>{fullName}</div>
                        <div className={classes.experence}><span>{workMonthExp} months ago</span> {sector}</div>
                        <div className={classes.desc}>{description}</div>
                        <div className={classes.vote}>
                            {vote}
                        </div>
                    </div>
                </div>
                <div className={classes.score}>
                    <ParticipantScore up={thumbUp} down={thumbDown}/>
                </div>
            </div>
    
        );
    }    
};

export default Participant;