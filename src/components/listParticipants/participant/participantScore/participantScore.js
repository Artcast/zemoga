import React from 'react';
import classes from './participantScore.module.scss';
import Score from './score/score';

const ParticipantScore = (props) => {
    const total = props.up + props.down;
    const perUp = Math.round((props.up / total) * 100);
    const perDown = Math.round((props.down / total) * 100);       
    return (
        <div className={classes.participantScore}>
            <Score thumbWay='up' per={perUp} />
            <Score thumbWay='down' per={perDown} />
        </div>

    );
};

export default ParticipantScore;