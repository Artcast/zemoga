import React from 'react';
import classes from './score.module.scss';
import Thumb from '../../../../thumb/thumb';

const Score = (props) => {
    const thumb = {
        up: classes.up,
        down:classes.down
    }
    const style = {
        width: `${props.per}%`
    }
    return (
        <div style={style} className={thumb[props.thumbWay]}>
            <Thumb thumbWay={props.thumbWay} />
            <div className={classes.percent}>{props.per}<span>%</span></div>
        </div>
    );
};
export default Score;