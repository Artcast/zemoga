import React from 'react';
import classes from './listParticipants.module.scss';
import Participant from './participant/participant';
import axios from 'axios';

class ListParticipants extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lParticipants: []
        }

    }

    componentDidMount() {
        const lParticipants = JSON.parse(localStorage.getItem('lParticipants'));
        if (lParticipants === null) {
            axios.get(`jsons/participants.json`)
            .then(res => {
                    const lParticipants = res.data;
                    localStorage.setItem('lParticipants', JSON.stringify(lParticipants));
                    this.setState({ lParticipants: lParticipants });
            });
        }
        else{
            this.setState({ lParticipants: lParticipants });
        }
            
    }

    render() {
        const { lParticipants } = this.state;

        const lParticipantsCards = lParticipants.length > 0 ? lParticipants.map((participant, index) => <Participant key={index} participant={participant} index={index} />) : null;
        return (
            <div className={classes.listParticipants}>
                <div className={classes.title}>Vote</div>
                <div className={classes.listContainer}>
                    {lParticipantsCards}
                </div>
            </div>
        );
    }

};

export default ListParticipants;
