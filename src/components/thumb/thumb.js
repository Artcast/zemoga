import React from 'react';
import classes from './thumb.module.scss';
import thumbImg from '../../assets/images/thumb.png';

const Thumb = (props) => {
    const thumb = {
        up: classes.up,
        down:classes.down
    }
    return (
        <div className={thumb[props.thumbWay]}>
            <img src={thumbImg} alt="Thumb" title="Thumb" />
        </div>

    );
};

export default Thumb;
