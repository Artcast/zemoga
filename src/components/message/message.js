import React from 'react';
import classes from './message.module.scss';

const Message = () => {
    return (
        <div className={classes.message}>
            <div className={classes.left}>
                <div className={classes.title}>Speak out. Be heard.</div>
                <div className={classes.subtitle}>Be Counted</div>
            </div>
            <div className={classes.right}>Rule of Thumb is a crowd sourced court of public opinion where anyone and everyone can speak out and speak freely. It’s easy: You share your opinion, we analyze and put the data in a public report.</div>
            <div className={classes.close}>×</div>
        </div>
    );
};

export default Message;
