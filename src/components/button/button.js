import React from 'react';
import classes from './button.module.scss';

const Button = (props) => {   
    return (
        <div onClick={props.onClick} className={classes.button}>
            {props.label}
        </div>

    );
};

export default Button;
