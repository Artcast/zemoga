import React from 'react';
import classes from './buttonThumb.module.scss';
import Thumb from '../../components/thumb/thumb';

const ButtonThumb = (props) => {
    const thumb = {
        up: classes.up,
        down:classes.down
    }
    const clas = [thumb[props.thumbWay]];
    if(props.border){
        clas.push(classes.border);
    }
    return (
        <div onClick={props.onClick} className={clas.join(' ')}>
            <Thumb thumbWay={props.thumbWay}/>
        </div>

    );
};

export default ButtonThumb;
