import React from 'react';
import twitter from '../../assets/images/twitter.png';
import facebook from '../../assets/images/facebook.png';
import classes from './footer.module.scss';

const Footer = (props) => {
    return (
        <footer>
            <div className={classes.footer}>
                <div className={classes.footerContainer}>
                    <div className={classes.left}>
                        <div>Terms and Conditions</div>
                        <div>Privacy Policy</div>
                        <div>Contact Us</div>
                    </div>
                    <div className={classes.right}>
                        <div>Follow Us</div>
                        <div><img src={twitter} alt="twitter" title="twitter" /></div>
                        <div><img src={facebook} alt="facebook" title="facebook" /></div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
