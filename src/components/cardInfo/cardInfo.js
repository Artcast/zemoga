import React from 'react';
import classes from './cardInfo.module.scss';
import wiki from '../../assets/images/wiki.png';
import Thumb from '../thumb/thumb';

const CardInfo = () => {
    return (
        <div className={classes.card}>
            <div className={classes.bg}></div>
            <div className={classes.cardInfo}>
                <div className={classes.title}>What’s your opinion on</div>
                <div className={classes.subtitle}>Pope Francis?</div>
                <div className={classes.desc}>He’s talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up) </div>
                <div className={classes.wiki}><img src={wiki} alt="Wiki" title="Wiki" />More Information</div>
                <div className={classes.veredict}>What is your veredict?</div>
            </div>
            <div className={classes.thumbContainer}>
                <div className={classes.thumbUp}>
                    <Thumb thumbWay="up" />
                </div>
                <div className={classes.thumbDown}>
                    <Thumb thumbWay="down" />
                </div>
            </div>
        </div>
    );
};

export default CardInfo;
